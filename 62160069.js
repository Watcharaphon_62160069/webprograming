var mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const alert = require('alert');

const { redirect } = require('express/lib/response');
let server = express();
server.use(bodyParser.json());
server.use(morgan('dev'));
server.use(cors());
server.use(bodyParser.urlencoded({ extended: true }))
server.use(express.static('html'));
server.set('views', __dirname + '/html');
server.engine('html', require('ejs').renderFile);

let user = {}

server.listen(3000, function () {
  console.log('Server Listen at http://localhost:3000');
});

var con = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  database: 'std_probation',
  password: ''
});

con.connect((err) => {
  if (err) {
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
  server.get('/', function (req, res) {
    res.sendFile(__dirname + "/html/" + "Login.html");
  })
});

////////////////////////////////////

server.get('/login', function (req, res) {
  user = {}
  let email = req.query.Email
  let password = req.query.Password

  con.query('SELECT * FROM mst_security WHERE user = ? AND password = ?',[email,password], function(err, data){
    
    if(err){
      return err;
    }
    if(data.length == 0){
      return res.sendFile(__dirname + "/html/nonmember.html");
    }
    if(data[0] == null){
      return res.redirect('/login')
    }else{
      con.query('SELECT * FROM `mst_lecture` WHERE `id_employee` = ?', [data[0].id_employee], function(err,data2){
        console.log(data2[0])
        user = {
          data: data2[0]
        }
      res.redirect('/main')
      })
    }
  })
})

server.get('/main', function(req, res, next){
  login = {}
  const id = user.data.id_employee;
  console.log([id])
  con.query('INSERT INTO trn_login (id_employee, datetime_login) VALUES(?, NOW())', id, function(err,res){
    if(err) return err;
  })
  res.render(__dirname + "/html/MainMenu.html",{
    id: user.data.id_employee,
    name: user.data.name,
    surname: user.data.surname,
  })
})

server.get('/', function(req, res){
  res.sendFile(__dirname + "/html/Login.html")
})

server.get('/logout', function(req, res, next){
  const id = user.data.id_employee
  console.log(id)
  con.query('INSERT INTO trn_logout (id_employee, datetime_logout) VALUES(?, NOW())', id, function(err,res){
    if(err) return err;
  })
  res.redirect('/')
})

// //search data

server.get('/data', function (req, res) {
  con.query('SELECT * FROM mst_student_pro;', function (err, rows) {
    if (err) {
      console.log(err);
      return res.redirect('/main')
    }
    res.render('Search.html', {
      data: rows
    })
  })
})

server.get('/search/:word', function (req, res, next) {
  let { word } = req.params
  search = "'%"+word+"%'"
  console.log(search)
  let query =
    'SELECT * FROM mst_student_pro WHERE Name LIKE ' +search +' OR Surname LIKE ' +search +' OR GPA LIKE ' +search
  con.query(query, (err, data) => {
    if (err) throw err
    res.render('Search.html', { data: data })
  })
})

// //add data
// server.post('/addEmployee', function (req, res) {
//   let lastID = 0
//   let name = req.body.fname
//   let surname = req.body.fsurname
//   let position = req.body.fposition
//   let salary = req.body.fsalary
//   let totalsale = req.body.ftotalsale
//   let email = req.body.femail
//   let password = req.body.fpassword
//   if (
//     name == '' ||
//     surname == '' ||
//     position == '' ||
//     salary == 0 ||
//     totalsale == 0 ||
//     email == '' ||
//     password == ''
//   ) {
//     return res.redirect('/main')
//   } else {
//     const mst_employee = {
//       name: name,
//       surname: surname,
//       position: position,
//       salary: salary,
//       total_sale: totalsale
//     }
//     con.query('INSERT INTO mst_employee SET ?', mst_employee, (err, res1) => {
//       if (err) throw err
//       lastID = res1.insertId
//       console.log('Last insert ID :'+ lastID + ' NewEmployeeName : ' + name + ' ' + surname)
//     })
//     let NewIDemployee = {}
//     con.query(
//       'SELECT id_employee FROM mst_employee Where name = ? AND surname = ? AND position = ?',
//       [name, surname, position],
//       (err, rows) => {
//         if (err) throw err
//         NewIDemployee = rows[rows.length - 1].id_employee
//         console.log('ID ที่ถูกเพิ่มล่าสุด : ' + NewIDemployee)
//         security = {
//           user: email,
//           password: password,
//           id_employee: NewIDemployee
//         }
//         con.query('INSERT INTO mst_security SET ?', security, (err, res2) => {
//           if (err) throw err
//           lastID = res2.insertId
//           console.log('Last ID : ', lastID)
//         })
//       }
//     )
//     res.redirect('/main')
//   }
// })

// //edit Employee

server.get('/EditPro/:id', function (req, res) {
  let editId = req.params.id;
  editEmp = editId;
  console.log('Edit Id From Database : '+editId);
  con.query(
    'SELECT * FROM mst_student_pro WHERE std_id = ?;',
    editId,
    (err, result) => {
      if (err) throw err

      if (result.length > 0) {
        Edite = result[0]
        console.log(Edite)
      }
      res.render(__dirname + '/html/Edit.html', {
        EditePro:{
          Id:Edite.Id_std,
          stdId : Edite.std_id,
          name:Edite.Name,
          surname:Edite.Surname,
          Gpa:Edite.GPA,
          Total:Edite.TotalCredit,
          year:Edite.Year,
          status:Edite.Status,
          round:Edite.Round
          
        }
      })
    }
  )
})

// server.get('/Cancel', function (req, res) {
//   res.redirect('/main')
// })

// server.post('/Cancel', function (req, res) {
//   res.redirect('/main')
// })

// server.post('/editEmployee/:id', function (req, res) {
//   let editId = req.params.id;

//   var employee = {
//     name: req.body.fname,
//     surname: req.body.fsurname,
//     position: req.body.fposition,
//     salary: req.body.fsalary,
//     total_sale: req.body.ftotalsale
//   }
//   con.query(
//     'UPDATE mst_employee SET ? WHERE id_employee = ?',
//     [employee, editId],
//     (err, res2) => {
//       if (err) throw err
//       var security = {
//         user: req.body.femail,
//         password: req.body.fpassword
//       }
//       con.query(
//         'UPDATE mst_security SET ? WHERE id_employee = ?',
//         [security, editId],
//         (err, res3) => {
//           if (err) throw err
//           return res.status(200)
//         }
//       )
//       console.log('Update success');
//     }
//   )
//   return res.redirect('/main')
// })

// //delete Employee
// server.get('/delete/:id', function (req, res) {
//   let deleteId = req.params.id;
//   console.log("Delete ID : " + deleteId)
//   con.query('SELECT * FROM mst_employee WHERE id_employee = ?', [deleteId],
//     (err, resultI) => {
//       if (err) throw err
//       res.render(__dirname + '/public/deleteEmployee.html', {
//         id: resultI[0].id_employee,
//         name: resultI[0].name,
//         surname: resultI[0].surname,
//         position: resultI[0].position,
//       })
//     })
// })

// server.get('/deleteEmployee/:id', function (req, res) {
//   var deleteId = req.params.id
//   console.log("You want to Delete ID : " + deleteId)

//   con.query('DELETE FROM trn_logout WHERE id_employee = ?',deleteId,(err, result) => {
//     if (err) throw err
//     console.log(`Deleted ${result.affectedRows} row(s)`)
//   })
//   con.query('DELETE FROM trn_login WHERE id_employee = ?',deleteId,(err, result) => {
//       if (err) throw err
//       console.log(`Deleted ${result.affectedRows} row(s)`)
//   })
//   con.query('DELETE FROM mst_security WHERE id_employee = ?',deleteId,(err, result) => {
//     if (err) throw err
//     console.log(`Deleted ${result.affectedRows} row(s)`)
//   })
//   con.query('DELETE FROM mst_employee WHERE id_employee = ?',deleteId,(err, result) => {
//     if (err) throw err
//     console.log(`Deleted ${result.affectedRows} row(s)`)
//   })
//   res.redirect('/main')
// })
